import json
from django.http import HttpResponse
from rest_framework.views import exception_handler

import logging
logger = logging.getLogger(__name__)


class SatelliteException(Exception):
    def __init__(self, cause, traceback=''):
        self.traceback = traceback
        self.cause = cause

    def __str__(self):
        return '{origin}\nFrom {parent}'.format(origin=self.traceback,
                                                parent=self.cause)


def boatload_exception_handler(exception, context):
    response = exception_handler(exception, context)

    if isinstance(exception, SatelliteException):
        logger.exception("Satellite Exception occurred", exception)
        pass

    try:
        detail = response.data['detail']
    except AttributeError:
        detail = exception.message
    response = HttpResponse(
        json.dumps({'detail': detail}),
        content_type="application/json", status=500
    )
    return response