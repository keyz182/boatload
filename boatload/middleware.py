import json
import sys
import traceback
import logging

from django.http import HttpResponse
from rest_framework import status

from boatload.settings import SATELLITE_MODE

# Get an instance of a logger
logger = logging.getLogger(__name__)

class ExceptionMiddleware(object):

    def process_exception(self, request, exception):
        exc_info = sys.exc_info()
        tb = traceback.format_exception(*(exc_info))

        retData = {'error': True, 'type':type(exception).__name__, 'traceback':tb}
        if 'message' in exception:
            retData['error'] = exception.message

        try:
            retData['request'] = repr(request)
        except:
            pass

        if SATELLITE_MODE:
            retData['location'] = "satellite"
        else:
            retData['location'] = "api"

        logger.exception("Intercepted exception")

        return HttpResponse(json.dumps(retData), status=status.HTTP_500_INTERNAL_SERVER_ERROR)