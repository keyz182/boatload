from django.db import models


class Job(models.Model):
    container_id = models.CharField(max_length=128, blank=False, primary_key=True)
    created = models.DateTimeField(auto_now_add=True)
    image_name = models.CharField(max_length=256, blank=False)
    image_tag = models.CharField(max_length=256, blank=True)
    host = models.CharField(max_length=256, null=True, blank=True)
    host_ip = models.CharField(max_length=16, null=True, blank=True)
    scriptname = models.CharField(max_length=256, null=True, blank=True)
    scripturl = models.CharField(max_length=4096, null=True, blank=True)
    dataurl = models.CharField(max_length=4096, null=True, blank=True)
    datapath = models.CharField(max_length=1024, null=True, blank=True)
    container_args = models.CharField(max_length=1024, null=True, blank=True)
    restart = models.CharField(max_length=32, null=True, blank=True)
    restartsec = models.CharField(max_length=32, null=True, blank=True)
    ports = models.CharField(max_length=256, null=True, blank=True)

    is_deleted = models.BooleanField(default=False)

    owner = models.ForeignKey(
        'auth.User', related_name='jobs')

    class Meta:
        ordering = ('created',)
