__author__ = 'keyz'
from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns

from fleetmanager.views import \
    Jobs, Script, StdOut, Metrics, Changes, Archive, GetFile, State, GetFileByNum, Top

urlpatterns = [
    url(r'^$', Jobs.as_view()),
    url(r'^(?P<container_id>[\w.@-_]+)/$', Jobs.as_view()),
    url(r'^(?P<container_id>[\w.@-_]+)/script$', Script.as_view()),
    url(r'^(?P<container_id>[\w.@-_]+)/state$', State.as_view()),
    url(r'^(?P<container_id>[\w.@-_]+)/stdout$', StdOut.as_view()),
    url(r'^(?P<container_id>[\w.@-_]+)/metrics$', Metrics.as_view()),
    # url(r'^(?P<container_id>[\w.@-_]+)/top', Top.as_view()),
    url(r'^(?P<container_id>[\w.@-_]+)/changes$', Changes.as_view()),
    url(r'^(?P<container_id>[\w.@-_]+)/file$', GetFile.as_view()),
    url(r'^(?P<container_id>[\w.@-_]+)/file/(?P<filepath>.*)?', GetFile.as_view()),
    url(r'^(?P<container_id>[\w.@-_]+)/filebynum/(?P<filenum>\d+)$', GetFileByNum.as_view()),
    url(r'^(?P<container_id>[\w.@-_]+)/archive$', Archive.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
