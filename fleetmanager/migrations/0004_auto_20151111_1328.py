# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('fleetmanager', '0003_auto_20151104_1436'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='job',
            name='data',
        ),
        migrations.RemoveField(
            model_name='job',
            name='http_forward',
        ),
        migrations.RemoveField(
            model_name='job',
            name='script',
        ),
        migrations.AddField(
            model_name='job',
            name='datapath',
            field=models.CharField(blank=True, null=True, max_length=1024),
        ),
        migrations.AddField(
            model_name='job',
            name='dataurl',
            field=models.CharField(blank=True, null=True, max_length=4096),
        ),
        migrations.AddField(
            model_name='job',
            name='scripturl',
            field=models.CharField(blank=True, null=True, max_length=4096),
        ),
    ]
