# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Job',
            fields=[
                ('container_id', models.CharField(max_length=128, serialize=False, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('image_name', models.CharField(max_length=256)),
                ('image_tag', models.CharField(max_length=256, blank=True)),
                ('host', models.CharField(null=True, max_length=256, blank=True)),
                ('scriptname', models.CharField(null=True, max_length=256, blank=True)),
                ('script', models.TextField(null=True, blank=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('owner', models.ForeignKey(to=settings.AUTH_USER_MODEL, related_name='jobs')),
            ],
            options={
                'ordering': ('created',),
            },
        ),
    ]
