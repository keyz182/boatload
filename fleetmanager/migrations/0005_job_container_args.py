# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('fleetmanager', '0004_auto_20151111_1328'),
    ]

    operations = [
        migrations.AddField(
            model_name='job',
            name='container_args',
            field=models.CharField(blank=True, max_length=1024, null=True),
        ),
    ]
