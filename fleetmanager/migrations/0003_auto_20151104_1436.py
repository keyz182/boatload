# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('fleetmanager', '0002_job_http_forward'),
    ]

    operations = [
        migrations.AddField(
            model_name='job',
            name='data',
            field=models.FileField(upload_to='boatload_data', null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='job',
            name='script',
            field=models.FileField(upload_to='boatload_scripts', null=True, blank=True),
        ),
    ]
