from time import sleep

import etcd

__author__ = 'keyz'
# from builtins import Exception, next
import uuid

from fleet.v1.client import *
from django.template import loader

from fleetmanager.models import Job
from boatload import settings

from boatload.settings import ETCDHOST, ETCDPATH, ETCDPORT
##Fleet
# GET /container/<id> - Get Info
# GET /container/ - List containers
# POST /container - Create Job
# - Create unit file from template, use django templates.
# - Name with uuid?
# - Stick info in etcd
# GET /container/<id>/state - Get State
# - Get using container id
# DELETE /container/<id> - Stop container
# - delete unit via id
# - remove from etcd


class ContainerException(Exception):
    """
    There was some problem generating or launching a docker container
    for the user
    """
    pass


def build_unit_file(name, image, extra_svc, datapath, container_args, restart_opts, ports):
    docker_opts = ''
    extra_unit_opts = ''
    x_fleet_opts = ''

    if extra_svc is not None:
        if datapath is None:
            datapath = '/data/'
        docker_opts = docker_opts + ' -v /data/' + name + ':' + datapath

    template = loader.get_template('fleetmanager/units/template.service')
    ctx = {
        'name': name,
        'image': image,
        'extra_svc': extra_svc,
        'extra_unit_opts': extra_unit_opts,
        'docker_opts': docker_opts,
        'ports': ports,
        'container_args': container_args,
        'restart_opts': restart_opts,
        'x_fleet_opts': x_fleet_opts,
        'docker_cmd': settings.DOCKER_COMMAND
    }

    return template.render(ctx)


def get_host_ip(id):
    fleet_client = Client(settings.FLEETAPI)

    for machine in fleet_client.list_machines():
        if machine.id == id:
            return machine.primaryIP

    return None

def create_job_fleet(image_name='', image_tag='', scriptname='', scripturl=None, dataurl=None, datapath=None,
                     container_args=None, restart=None, restartsec=None, ports=None):
    u = uuid.uuid4().hex
    fleet_client = Client(settings.FLEETAPI)

    ##TODO: use http_forward to populate etcd /proxy/proxies/containername to utilise configurable_http_proxy

    ##TODO: Check if image is allowed

    ##TODO: Figure out how best to xfer script to container

    # Render Service file
    image = "%s:%s" % (image_name, image_tag)

    extra_svc = None

    restart_opts = ''

    if restart is not None:
        restart_opts = ''.join([restart_opts, "Restart=%s" % restart])

    if restartsec is not None:
        restart_opts = '\n'.join([restart_opts, "RestartSec=%s" % restartsec])

    if ports is None:
        ports = ''

    if dataurl is not None and datapath is not None and scripturl is not None:
        ctx = {
            'scriptname': scriptname,
            'scripturl': scripturl,
            'dataurl': dataurl,
            'containerid': u
        }
        template = loader.get_template('fleetmanager/units/_data_service_template')
        extra_svc = template.render(ctx)
        pass

    service = build_unit_file(name=u, image=image, extra_svc=extra_svc, datapath=datapath,
                              container_args=container_args, restart_opts=restart_opts, ports=ports)

    ##TODO: submit service file   ##TODO: load service file   ##TODO: get service host
    unit = Unit(desired_state='loaded', from_string=service)
    deployedunit = None
    machineid = None
    host_ip = None
    name = '%s.service' % u

    try:
        ##TODO: takes time to propegate, loop/poll?
        deployedunit = fleet_client.create_unit(name, unit)
        sleep(10)
        state = next(fleet_client.list_unit_states(unit_name=name))
        machineid = state.machineID

        etc = etcd.Client(host=ETCDHOST, port=ETCDPORT)
        path = "".join([ETCDPATH, machineid])

        satellite = etc.read(path)

        host_ip = satellite.value.split(':')[0]
    except APIError as exc:
        print('Unable to create unit: {0}'.format(exc))
        raise ContainerException  ##TODO: more relevent error

    data = {"container_id": u, "image_name": image_name, "image_tag": image_tag, "host": machineid, 'host_ip':host_ip}

    #TODO: if expose=True, point proxy to host_ip?

    return data


def start_job_fleet(container_id):
    name = '%s.service' % container_id

    fleet_client = Client(settings.FLEETAPI)

    try:
        unit = fleet_client.get_unit(name)
        unit.set_desired_state('launched')
    except APIError as exc:
        if exc.code == 404:
            print('Unit %s does not exist.' % name)
        else:
            print('Unable to get unit: {0}'.format(exc))


def get_job_state_fleet(container_id):
    name = '%s.service' % container_id
    fleet_client = Client(settings.FLEETAPI)

    try:
        unit = next(fleet_client.list_unit_states(unit_name=name))

        state = {
            'name': container_id,
            'loadState': unit.systemdLoadState,
            'activeState': unit.systemdLoadState,
            'subState': unit.systemdLoadState
        }

        return state

    except APIError as exc:
        print('Unable to create unit: {0}'.format(exc))
        raise ContainerException  ##TODO: more relevent error


def get_all_job_states(jobs):
    fleet_client = Client(settings.FLEETAPI)

    try:
        states = {}
        for job in jobs:
            name = '%s.service' % job.container_id

            unit = next(fleet_client.list_unit_states(unit_name=name))

            states[unit.name] = {
                'loadState': unit.systemdLoadState,
                'activeState': unit.systemdLoadState,
                'subState': unit.systemdLoadState
            }

        return states

    except APIError as exc:
        print('Unable to create unit: {0}'.format(exc))
        raise ContainerException  ##TODO: more relevent error


def delete_job_fleet(container_id):
    name = '%s.service' % container_id

    fleet_client = Client(settings.FLEETAPI)

    try:
        unit = fleet_client.get_unit(name)
        unit.destroy()
        container = Job.objects.get(container_id=container_id)
        container.is_deleted = True

        container.save()
    except APIError as exc:
        print('Unable to create unit: {0}'.format(exc))
        raise ContainerException  ##TODO: more relevent error
