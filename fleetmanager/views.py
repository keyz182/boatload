from requests import HTTPError

__author__ = 'keyz'
import json
import sys
import traceback

try:
    from StringIO import StringIO
except ImportError:
    from io import StringIO
try:
    import urllib.parse
except ImportError:
    from urlparse import urlparse

from django.http import HttpResponseRedirect, HttpResponse
from django.http import FileResponse

from fleetmanager.models import Job
from fleetmanager.serializers import JobSerializer

from rest_framework.response import Response
from rest_framework import permissions
from rest_framework import status
from rest_framework import mixins
from rest_framework import generics

from fleetmanager.fleettools import create_job_fleet, start_job_fleet, get_job_state_fleet, delete_job_fleet
from fleetmanager.satellitetools import get_stdout, get_metrics, get_changes, get_changed_file, get_archive, get_top
from fleetmanager.filters import IsOwnerFilterBackend, IsNotDeletedFilterBackend


##Fleet
# GET /container/<id> - Get Info
# GET /container/ - List containers
# POST /container - Create Job
# - Create unit file from template, use django templates.
# - Name with uuid?
# - Stick info in etcd
# DELETE /container/<id> - Stop container
# - delete unit via id
# - remove from etcd
class Jobs(mixins.ListModelMixin,
           mixins.CreateModelMixin,
           mixins.RetrieveModelMixin,
           mixins.DestroyModelMixin,
           generics.GenericAPIView):
    """
    To create a Job, you must POST scriptname, script,
    image_name and image_tag (all strings).

    You will receive an ID in return.

    GET /container/ - List containers

    POST /container - Create Job

    - Create unit file from template, use django templates.

    - Name with uuid?

    - Stick info in etcd

    GET /container/&lt;container id&gt; - Get Info

    DELETE /container/&lt;container id&gt; - Stop container

    - delete unit via id

    - remove from etcd

     - DELETE /v1/container/<container id>/ - Delete a container
     - GET /v1/container/<containerID>/ - Get Container Info
     - GET /v1/container/<containerID>/stdout - Get Container Output
     - GET /v1/container/<containerID>/script - Get script
     - GET /v1/container/<containerID>/top - Get top output
     - GET /v1/container/<containerID>/state - Get state
     - GET /v1/container/<containerID>/metrics - Get Container Metrics
     - GET /v1/container/<containerID>/changes - Get a list of changed files in the Container
     - GET /v1/container/<containerID>/file - Get a list of changed files in the Container
     - GET /v1/container/<containerID>/file/<filepath> - Get a specific file from the Container
     - GET /v1/container/<containerID>/filebynum/<filenum> - Get a specific file from the Container (by position in array)
     - GET /v1/container/<containerID>/archive - Get full changes archive
    """
    queryset = Job.objects.all()
    serializer_class = JobSerializer
    permission_classes = (permissions.IsAuthenticated,)
    filter_backends = (IsOwnerFilterBackend, IsNotDeletedFilterBackend,)
    lookup_field = "container_id"

    def perform_destroy(self, instance):
        delete_job_fleet(instance.container_id)
        instance.is_deleted = True
        instance.save()

    def perform_create(self, serializer, owner):
        obj = serializer.save(owner=owner)
        start_job_fleet(obj.container_id)

    def get(self, request, *args, **kwargs):
        if "container_id" in kwargs:
            return self.retrieve(request, *args, **kwargs)
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = request.data.copy()

        if 'scriptname' not in data:
            data['scriptname'] = 'script.py'

        if 'image_name' not in data:
            return Response("No image_name",
                            status=status.HTTP_400_BAD_REQUEST)

        if 'image_tag' not in data:
            return Response("No image_tag",
                            status=status.HTTP_400_BAD_REQUEST)

        if 'dataurl' not in data:
            data['dataurl'] = None
        if 'datapath' not in data:
            data['datapath'] = None
        if 'scripturl' not in data:
            data['scripturl'] = None
        if 'container_args' not in data:
            data['container_args'] = None
        if 'restart' not in data:
            data['restart'] = None
        if 'restartsec' not in data:
            data['restartsec'] = None
        if 'ports' in data:
            p = data['ports'].split(',')
            data['ports'] = '-p ' + ' -p '.join(p)
        else:
            data['ports'] = None

        jobdata = create_job_fleet(image_name=data['image_name'], image_tag=data['image_tag'],
                                   scriptname=data['scriptname'], scripturl=data['scripturl'],
                                   dataurl=data['dataurl'], datapath=data['datapath'],
                                   container_args=data['container_args'], restart=data['restart'],
                                   restartsec=data['restartsec'], ports=data['ports'])

        data.update(jobdata)

        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer, self.request.user)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)


class State(generics.GenericAPIView):
    queryset = Job.objects.all()
    serializer_class = JobSerializer
    permission_classes = (permissions.IsAuthenticated,)
    filter_backends = (IsOwnerFilterBackend,)
    lookup_field = "container_id"

    def get(self, request, *args, **kwargs):
        instance = self.get_object()

        output = {'fleet_state':get_job_state_fleet(instance.container_id), 'docker_state':''}

        try:
            metrics = get_metrics(instance.container_id, raw=True)
            if "metrics" in metrics:
                if "State" in metrics['metrics']:
                    output['docker_state'] = metrics['metrics']['State']

            return Response(output)
        except Exception as e:
            return Response({"error": sys.exc_info()[1], "traceback": traceback.format_exc()},
                            status=status.HTTP_400_BAD_REQUEST)


class Script(generics.GenericAPIView):
    queryset = Job.objects.all()
    serializer_class = JobSerializer
    permission_classes = (permissions.IsAuthenticated,)
    filter_backends = (IsOwnerFilterBackend,)
    lookup_field = "container_id"

    def get(self, request, *args, **kwargs):
        instance = self.get_object()
        return HttpResponseRedirect(instance.script.url)


class Data(generics.GenericAPIView):
    queryset = Job.objects.all()
    serializer_class = JobSerializer
    permission_classes = (permissions.IsAuthenticated,)
    filter_backends = (IsOwnerFilterBackend,)
    lookup_field = "container_id"

    def get(self, request, *args, **kwargs):
        instance = self.get_object()
        return HttpResponseRedirect(instance.data.url)


# ##DockerSatellite
# - Get using container id
# GET /container/<id>/stdout - Get StdOut
# GET /container/<id>/metrics - Get Metric
# GET /container/<id>/changes - Get Changes POST ```{"path":"filepath"} to get a file
# GET /container/<id>/changes/filenumorpath? - Get file at path

class StdOut(generics.GenericAPIView):
    queryset = Job.objects.all()
    serializer_class = JobSerializer
    permission_classes = (permissions.IsAuthenticated,)
    filter_backends = (IsOwnerFilterBackend,)
    lookup_field = "container_id"

    def get(self, request, *args, **kwargs):
        instance = self.get_object()
        return get_stdout(instance.container_id)


class Metrics(generics.GenericAPIView):
    queryset = Job.objects.all()
    serializer_class = JobSerializer
    permission_classes = (permissions.IsAuthenticated,)
    filter_backends = (IsOwnerFilterBackend,)
    lookup_field = "container_id"

    def get(self, request, *args, **kwargs):
        instance = self.get_object()
        return get_metrics(instance.container_id)


class Top(generics.GenericAPIView):
    queryset = Job.objects.all()
    serializer_class = JobSerializer
    permission_classes = (permissions.IsAuthenticated,)
    filter_backends = (IsOwnerFilterBackend,)
    lookup_field = "container_id"

    def get(self, request, *args, **kwargs):
        instance = self.get_object()
        return get_top(instance.container_id)


class Changes(generics.GenericAPIView):
    queryset = Job.objects.all()
    serializer_class = JobSerializer
    permission_classes = (permissions.IsAuthenticated,)
    filter_backends = (IsOwnerFilterBackend,)
    lookup_field = "container_id"

    def get(self, request, *args, **kwargs):
        instance = self.get_object()
        return get_changes(instance.container_id)


def build_file_response_or_error(r):
    if 'error' in r:
        return Response(r['error'], status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    ctype = r.headers['content-type']
    cdisp = r.headers['content-disposition']
    raw_data = r.raw
    response = FileResponse(raw_data, content_type=ctype)
    response['Content-Disposition'] = cdisp
    return response


class GetFileByNum(generics.GenericAPIView):
    queryset = Job.objects.all()
    serializer_class = JobSerializer
    permission_classes = (permissions.IsAuthenticated,)
    filter_backends = (IsOwnerFilterBackend,)
    lookup_field = "container_id"

    def get(self, request, filenum='0', *args, **kwargs):
        filenum = int(filenum)
        instance = self.get_object()
        out = get_changes(instance.container_id, raw=True)

        if isinstance(out, HttpResponse):
            return out

        path = out[filenum]['Path']
        return get_changed_file(instance.container_id, path)


class GetFile(generics.GenericAPIView):
    queryset = Job.objects.all()
    serializer_class = JobSerializer
    permission_classes = (permissions.IsAuthenticated,)
    filter_backends = (IsOwnerFilterBackend,)
    lookup_field = "container_id"

    def get(self, request, *args, **kwargs):
        instance = self.get_object()

        if 'path' in kwargs:
            filepath = kwargs['path']
            path = urllib.parse.unquote(filepath).decode('utf8')

            return get_changed_file(instance.container_id, path)
        else:
            instance = self.get_object()
            return get_changes(instance.container_id)

    def post(self, request, *args, **kwargs):
        data = request.data
        instance = self.get_object()
        if 'path' in data:
            return get_changed_file(instance.container_id, data['path'])
        else:
            return Response({"error": "No Path specified"}, status=status.HTTP_400_BAD_REQUEST)


class Archive(generics.GenericAPIView):
    queryset = Job.objects.all()
    serializer_class = JobSerializer
    permission_classes = (permissions.IsAuthenticated,)
    filter_backends = (IsOwnerFilterBackend,)
    lookup_field = "container_id"

    def get(self, request, *args, **kwargs):
        instance = self.get_object()
        return get_archive(instance.container_id)
