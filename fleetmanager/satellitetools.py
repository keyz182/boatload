import time
from requests.packages.urllib3.exceptions import HTTPError
from simplejson import JSONDecodeError

__author__ = 'keyz'

from django.http import FileResponse, HttpResponse
from django.http import JsonResponse
from rest_framework import status

import etcd
import requests

try:
    from StringIO import StringIO
except ImportError:
    from io import StringIO

from fleetmanager.models import Job
from boatload.settings import ETCDHOST, ETCDPATH, ETCDPORT

etcdclient = etcd.Client(host=ETCDHOST, port=ETCDPORT)

def etcdget(path, max=10):
    i = 0
    lasterr = None

    while i < max:
        try:
            return etcdclient.read(path, timeout=30)
        except etcd.EtcdKeyNotFound as e:
            lasterr = e

        time.sleep(3)

    raise lasterr


def satellite_get(template, id):
    job = Job.objects.get(container_id=id)
    path = "".join([ETCDPATH, job.host])

    satellite = etcdget(path)

    url = template % (satellite.value, job.container_id)

    return requests.get(url)


def response_or_raise(r, asStr=False):
    try:
        r.raise_for_status()
        if asStr:
            return HttpResponse(r.content)

        data = r.json()
        if "error" in data:
            return JsonResponse(data, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        return JsonResponse(data, safe=False)
    except HTTPError as e:
        return HttpResponse(r.content, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
    except JSONDecodeError as e:
        return HttpResponse(r.content)


def get_stdout(id):
    return response_or_raise(satellite_get(template="http://%s/v1/satellite/%s/stdout/", id=id), asStr=True)


def get_top(id):
    return response_or_raise(satellite_get(template = "http://%s/v1/satellite/%s/top/", id=id))


def get_metrics(id, raw=False):
    r = satellite_get(template = "http://%s/v1/satellite/%s/metrics/", id=id)

    if not raw:
        return response_or_raise(r)

    try:
        r.raise_for_status()
        return r.json()
    except HTTPError as e:
        return HttpResponse(r.content, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
    except JSONDecodeError as e:
        return HttpResponse(r.content, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


def get_changes(id, raw=False):
    r = satellite_get(template="http://%s/v1/satellite/%s/changes/", id=id)

    if not raw:
        return response_or_raise(r)

    try:
        r.raise_for_status()
        return r.json()
    except HTTPError as e:
        return HttpResponse(r.content, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
    except JSONDecodeError as e:
        return HttpResponse(r.content, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


def stream_response_or_err(r):
    try:
        ctype = r.headers['content-type']

        r.raise_for_status()
        response = FileResponse(r.raw, content_type=ctype)
        response['Content-Disposition'] = 'attachment; filename="data.tar"'
        return response
    except HTTPError as e:
        return HttpResponse(r.content, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
    except Exception as e:
        raise e

def get_archive(id):
    template = "http://%s/v1/satellite/%s/archive/"
    job = Job.objects.get(container_id=id)
    path = "".join([ETCDPATH, job.host])

    satellite = etcdget(path)

    url = template % (satellite.value, job.container_id)

    r = requests.get(url=url, stream=True)

    return stream_response_or_err(r)


def get_changed_file(id, file):
    template = "http://%s/v1/satellite/%s/changes/"
    job = Job.objects.get(container_id=id)
    path = "".join([ETCDPATH, job.host])

    satellite = etcdget(path)

    url = template % (satellite.value, job.container_id)
    payload = {"path": file}

    r = requests.post(url=url, data=payload, stream=True)

    return stream_response_or_err(r)