__author__ = 'keyz'
from django.contrib.auth.models import User
from rest_framework import serializers

from fleetmanager.models import Job


class UserSerializer(serializers.ModelSerializer):
    jobs = serializers.PrimaryKeyRelatedField(many=True, queryset=Job.objects.all())

    class Meta:
        model = User
        fields = ('id', 'username', 'jobs')


class JobSerializer(serializers.ModelSerializer):
    owner = serializers.ReadOnlyField(source='owner.username')

    class Meta:
        model = Job
        fields = (
            'container_id',
            'created',
            'image_name',
            'image_tag',
            'host',
            'host_ip',
            'ports',
            'scriptname',
            'scripturl',
            'dataurl',
            'datapath',
            'container_args',
            'restart',
            'restartsec',
            'owner')
