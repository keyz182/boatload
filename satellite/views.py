import sys
import traceback

import docker
from django.http import FileResponse
from requests import HTTPError
from rest_framework import permissions
from rest_framework import views
from rest_framework.response import Response
from rest_framework.status import HTTP_400_BAD_REQUEST

try:
    from StringIO import StringIO
except ImportError:
    from io import StringIO

from boatload.settings import BASE_DOCKER_URL


# Create your views here.
class ListOnHost(views.APIView):
    """
    Returns a list of all running docker containers.


    Sub Endpoints:


    - ./&lt;container id&gt;/metrics - *Returns metrics for the specified container.*
    - ./&lt;container id&gt;/stdout - *Returns the log output for the specified container.*
    - ./&lt;container id&gt;/changes - *Returns a list of changed files for the specified container. POST ```{"path":"filepath"} to get a file*
    - ./&lt;container id&gt;/changes/&lt;filename&gt; - *Returns the selected file from the specified container in base64 encoding.*
    """
    permission_classes = (permissions.AllowAny,)

    def get(self, request, *args, **kwargs):
        dcli = docker.Client(base_url=BASE_DOCKER_URL, version='auto')

        try:
            containers = dcli.containers()
        except Exception as e:
            return Response({"error": sys.exc_info()[1], "traceback": traceback.format_exc()})
        return Response({"containers": containers})


class Metrics(views.APIView):
    """
    Returns metrics for the specified container.
    """
    permission_classes = (permissions.AllowAny,)

    def get(self, request, id):
        dcli = docker.Client(base_url=BASE_DOCKER_URL, version='auto')

        metrics = dcli.inspect_container(id)
        stats = dcli.stats(id, True, False)

        return Response({"metrics": metrics, "stats":stats})


class Top(views.APIView):
    """
    Returns metrics for the specified container.
    """
    permission_classes = (permissions.AllowAny,)

    def get(self, request, id):
        dcli = docker.Client(base_url=BASE_DOCKER_URL, version='auto')

        top = dcli.top(id)

        return Response(top)


class StdOut(views.APIView):
    """
    Returns the log output for the specified container.
    """
    permission_classes = (permissions.AllowAny,)

    def get(self, request, id):
        dcli = docker.Client(base_url=BASE_DOCKER_URL, version='auto')

        data = dcli.logs(id)

        return Response(data.decode('utf-8'))


class Diff(views.APIView):
    """
    Returns a list of changed files for the specified container.
    """
    permission_classes = (permissions.AllowAny,)

    def get(self, request, id):
        dcli = docker.Client(base_url=BASE_DOCKER_URL, version='auto')

        data = dcli.diff(id)

        return Response(data, content_type='application/json')

    def post(self, request, id, *args, **kwargs):
        data = request.data
        if 'path' in data:
            dcli = docker.Client(base_url=BASE_DOCKER_URL, version='auto')

            data, stat = dcli.get_archive(id, data['path'])
            response = FileResponse(data, content_type='application/octet-stream')
            response['Content-Disposition'] = 'attachment; filename="data.tar"'
            return response

        return Response("No path specified",
                        status=HTTP_400_BAD_REQUEST)


class Download(views.APIView):
    """
    Returns the selected file from the specified container in base64 encoding.
    """
    permission_classes = (permissions.AllowAny,)

    def get(self, request, id, path):
        dcli = docker.Client(base_url=BASE_DOCKER_URL, version='auto')

        data, stat = dcli.get_archive(id, path)
        response = FileResponse(data, content_type='application/octet-stream')
        response['Content-Disposition'] = 'attachment; filename="data.tar"'
        return response


class DownloadArchive(views.APIView):
    """
    Returns the selected file from the specified container in base64 encoding.
    """
    permission_classes = (permissions.AllowAny,)

    def get(self, request, id):
        dcli = docker.Client(base_url=BASE_DOCKER_URL, version='auto')

        data,stat = dcli.get_archive(id, '/')
        response = FileResponse(data, content_type='application/x-tar')
        response['Content-Disposition'] = 'attachment; filename="data.tar"'
        return response
