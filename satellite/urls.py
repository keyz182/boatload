__author__ = 'keyz'
from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns

from satellite.views import ListOnHost, StdOut, Metrics, Diff, Download, DownloadArchive, Top

urlpatterns = [
    url(r'^$', ListOnHost.as_view()),
    url(r'^(?P<id>.+)/stdout/$', StdOut.as_view()),
    url(r'^(?P<id>.+)/metrics/$', Metrics.as_view()),
    url(r'^(?P<id>.+)/top/$', Top.as_view()),
    url(r'^(?P<id>.+)/changes/$', Diff.as_view()),
    url(r'^(?P<id>.+)/changes/(?P<file>\.+)/$', Download.as_view()),
    url(r'^(?P<id>.+)/archive/$', DownloadArchive.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
