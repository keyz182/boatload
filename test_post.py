import requests

DOCKER_API_URL = 'http://172.17.8.101:8080/v1/container/'

params = dict(
    format='json',
)

script = "https://www.dropbox.com/s/0f4a3ivyqnoum1z/test.sh?dl=1"
data = "https://www.dropbox.com/s/7jrks9nzeto54qk/test.zip?dl=1"
datapath = "/data123/"
scriptname = "test.sh"

payload = {
    "image_name": "ubuntu",
    "image_tag": "latest",
    "scriptname": scriptname,
    "scripturl": script,
    "dataurl": data,
    "datapath": datapath,
    "container_args": "sh " + datapath + scriptname
}

resp = requests.post(url=DOCKER_API_URL, params=params, data=payload, auth=('admin', 'admin'))

print(resp.text)
