from __future__ import print_function

import netifaces
import os
import sys
import traceback
from time import sleep

import etcd
import fleet.v1 as fleet
import requests

ETCDHOST = netifaces.gateways()['default'][netifaces.AF_INET][0]
ETCDPORT = 2379

ETCDKEY = os.getenv('ETCDKEY', "/services/satellite/")

FLEET = 'http+unix://%2Fvar%2Frun%2Ffleet.sock'

SATELLITEHOST = os.getenv('SATELLITE_PORT_7020_TCP_ADDR', None)
SATELLITEPORT = os.getenv('SATELLITE_PORT_7020_TCP_PORT', None)

COREOS_PRIVATE_IPV4 = os.getenv('COREOS_PRIVATE_IPV4', None)
COREOS_PUBLIC_IPV4 = os.getenv('COREOS_PUBLIC_IPV4', None)

if SATELLITEHOST is None or SATELLITEPORT is None or COREOS_PRIVATE_IPV4 is None:
    print("Satellite container not linked or COREOS host ip not in env")
    sys.exit(1)

print("SATELLITEHOST : " + str(SATELLITEHOST))
print("SATELLITEPORT : " + str(SATELLITEPORT))
print("COREOS_PRIVATE_IPV4 : " + str(COREOS_PRIVATE_IPV4))
print("COREOS_PUBLIC_IPV4 : " + str(COREOS_PUBLIC_IPV4))
print("ETCDHOST : " + str(ETCDHOST))
print("ETCDPORT : " + str(ETCDPORT))
print("ETCDKEY : " + str(ETCDKEY))
print("FLEET : " + str(FLEET))

while True:
    try:
        r = requests.get("http://" + SATELLITEHOST + ":" + SATELLITEPORT + "/v1/satellite/", timeout=10)
        # We don't care what state the API is in, just that it's there
        etc = etcd.Client(host=ETCDHOST, port=ETCDPORT)
        fleet_client = fleet.Client(FLEET)
        hostid = [m.id for m in fleet_client.list_machines() if
                  m.primaryIP == COREOS_PRIVATE_IPV4 or m.primaryIP == COREOS_PUBLIC_IPV4][0]
        key = ETCDKEY + hostid
        value = COREOS_PRIVATE_IPV4 + ":" + SATELLITEPORT
        print("Writing value '" + value + "' to key '" + key + "'")
        etc.write(key, value, ttl=30)
    except Exception as e:
        print(e)
        traceback.print_exc(file=sys.stdout)
    finally:
        sleep(20)
