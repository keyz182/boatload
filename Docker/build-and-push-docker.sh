#!/bin/bash

#Note, these need changing in build.sh files and dockerfiles if changed here
REPOSITORY=switcheu
TAG=latest


cd base
sh ./build.sh || { echo 'base failed' ; exit 1; }
docker push $REPOSITORY/boatload_base:$TAG || { echo 'base failed' ; exit 1; }
cd ../api
sh ./build.sh || { echo 'api failed' ; exit 1; }
docker push $REPOSITORY/boatload_api:$TAG || { echo 'api failed' ; exit 1; }
cd ../satellite
sh ./build.sh || { echo 'satellite failed' ; exit 1; }
docker push $REPOSITORY/boatload_satellite:$TAG || { echo 'satellite failed' ; exit 1; }
cd ../satellite-discovery
sh ./build.sh || { echo 'satellite-discovery failed' ; exit 1; }
docker push $REPOSITORY/boatload_satellite-discovery:$TAG || { echo 'satellite-discovery failed' ; exit 1; }
cd ..

