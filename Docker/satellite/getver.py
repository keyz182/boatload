from __future__ import print_function

import json
import socket
import sys

if len(sys.argv) is not 2:
    print("No socket specified", file=sys.stderr)
    sys.exit(2)

# Create a UDS socket
sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)

# Connect the socket to the port where the server is listening
server_address = sys.argv[1]
print('connecting to %s' % server_address, file=sys.stderr)

try:
    sock.connect(server_address)
    # Send data
    message = 'GET /version HTTP/1.1\r\n\r\n'
    print('sending "%s"' % message, file=sys.stderr)
    sock.sendall(message.encode('utf-8'))

    amount_received = 0
    amount_expected = 0

    lines = []
    curline = ''
    prevchar = ''
    char = ''

    while True:
        char = sock.recv(1)
        curline += char.decode('utf-8')
        if curline.endswith('\r\n'):
            lines.append(curline)
            if curline.startswith('Content-Length:'):
                conlen = curline[15:].lstrip()
                amount_expected = int(conlen)
            if curline == '\r\n':
                break
            print(curline, file=sys.stderr)
            curline = ''

    print("Getting %i bytes of data" % amount_expected, file=sys.stderr)

    data = b''
    while amount_received < amount_expected:
        din = sock.recv(1)
        amount_received += 1
        data += din

    parseddata = json.loads(data.decode('utf-8'))

    print(parseddata['Version'])

except socket.error as msg:
    print(msg, file=sys.stderr)
    sys.exit(1)
finally:
    print('closing socket', file=sys.stderr)
    sock.close()
