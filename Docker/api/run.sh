#!/bin/sh

if [ ! -z "$ADMINHASH" ]
then
    sed "s/##PWHASH##/$ADMINHASH/g" /app/data/data.json.template > /app/data/data.json
fi

/usr/bin/python /app/manage.py makemigrations --noinput
/usr/bin/python /app/manage.py migrate --noinput
/usr/bin/python /app/manage.py loaddata /app/data/data.json

/usr/bin/ssh-keygen -A

/usr/bin/supervisord -c /app/supervisord.conf